<center>
    <?php if(isset($_POST['update_web_settings'])) { $admin->updateWebSettings($_POST['title'],$_POST['footer']); } ?>
    <form method="POST">
        <div style="margin-bottom: 25px" class="input-group col-md-6">
            Svetainės title
            <input type="text" class="form-control" name="title" value="<?php echo $system->title; ?>">                                        
        </div>
        <div style="margin-bottom: 25px" class="input-group col-md-6">
            Svetainės pavadinimas
            <input type="text" class="form-control" name="footer" value="<?php echo $system->footer; ?>">                                        
        </div>
        <input type="submit" name="update_web_settings" value="Išsaugoti" class="btn btn-success"/>
        <hr>
    </form>
    <h3>Dizainas</h3>
    <hr>
    <div class="well well-sm">Jūs galite pasirinkti dizaino spalvą iš 5 skirtingų variantų.</div>
    <a href='index.php?id=admin_change_theme&item=default' class='btn btn-default'>Šviesus</a>
    <a href='index.php?id=admin_change_theme&item=danger' class='btn btn-danger'>Šviesiai raudonas</a>
    <a href='index.php?id=admin_change_theme&item=warning' class='btn btn-warning'>Šviesiai geltonas</a>
    <a href='index.php?id=admin_change_theme&item=info' class='btn btn-info'>Šviesiai mėlynas</a>
    <a href='index.php?id=admin_change_theme&item=primary' class='btn btn-primary'>Tamsiai mėlynas</a>
</center>