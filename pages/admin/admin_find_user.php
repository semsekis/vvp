<?php
$query = $system->PDO->prepare("SELECT * FROM `user` WHERE `username`=?");
$query->execute(array($item));
$row = $query->fetch(PDO::FETCH_ASSOC);

echo "
    <table class='table'>
        <thead>
            <tr>
                <th>#</th>
                <th>Vardas_Pavardė</th>
                <th>Užblokuotas</th>
                <th>Atblokuoti</th>
                <th>Trinti</th>
            </tr>
        </thead>
        <tbody>
";

if($row['banned'] > time()){
    $time = round(($row['banned'] - time()) / 60);
    $banned = "Taip, {$time}min.";   
}else{
     $banned = "Ne.";   
}
 echo "
            <tr>
                <th>{$row['id']}</th>
                <td>{$row['username']}</td>
                <td>{$banned}</td>
                <td><a href='index.php?id=admin_unban&item={$row['id']}'><span class='glyphicon glyphicon-ok'></span></a></td>
                <td><a href='index.php?id=admin_delete&item={$row['id']}'><span class='glyphicon glyphicon-remove'></span></a></td>
            </tr>
        </tbody>
    </table>
";
?>