<center>
    <?php if(isset($_POST['create_new'])) { $news->createNew($_POST['author'],$_POST['name'],$_POST['content']); } ?>
    <form method="POST">
        <div style="margin-bottom: 25px" class="input-group col-md-6">
            <input type="text" class="form-control" name="name" placeholder="Naujienos pavadinimas">                                        
        </div>
        <div style="margin-bottom: 25px" class="input-group col-md-6">
            <input type="text" class="form-control" name="author" placeholder="Naujienos autorius">                                        
        </div>
        <div style="margin-bottom: 25px" class="input-group col-md-6">
            <textarea rows="6" name="content" class="form-control" placeholder="Naujiena"></textarea>
        </div>
        <input type="submit" name="create_new" value="Kurti" class="btn btn-success"/>
    </form>
</center>
<hr>
<?php $admin->showNews(); ?>