<form method="POST">
    <center>
        <h4>Ieškoti žaidėją</h4>
        <div class="input-group col-md-6">
            <input type="text" class="form-control" name="username" placeholder="Vardas_Pavardė">
            <span class="input-group-btn">
                <button class="btn btn-default" name="find_user" type="submit">Ieškoti!</button>
            </span>
        </div>
    </center>
</form>
<hr>
<?php
if(isset($_POST['find_user'])) { $admin->findUser($_POST['username']); }
$admin->showUsers();
?>