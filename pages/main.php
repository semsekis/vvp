<?php
$user->getUserInfo();

echo "
    {$user->skin}
    <div style='position: relative; top: 5px; left: 35px;'>
        <span class='label label-primary'>Skin</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->skin_id}</span>
        <a href='index.php?id=change_skin'><span class='label label-warning' style='margin-left: -6px;'><span class='glyphicon glyphicon-wrench'></span></span></a>
        <br/><br/>
    </div>
    <div style='position: relative; left: 170px; top: -325px;'>
        <span class='label label-primary'>Vardas_Pavarde</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->username}</span>
        <a href='index.php?id=change_name'><span class='label label-warning' style='margin-left: -6px;'><span class='glyphicon glyphicon-wrench'></span></span></a>
        <br/><br/>
         
        <span class='label label-primary'>Pinigai</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->money}</span><br/><br/>
        
        <span class='label label-primary'>Darbas</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->job}</span></br/><br/>
        
        <span class='label label-primary'>Patirtis</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->xp}</span></br/><br/>
        
        <span class='label label-primary'>Pinigai banke</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->bank_money}</span></br/><br/>
        
        <span class='label label-primary'>Kreditai</span>
        <span class='label label-success' style='margin-left: -6px;'>{$user->credits}</span></br/><br/>
";
if($user->vip > time()) {  
    $liko_laiko = round(($user->vip - time() ) / 24 / 60 /60);
    echo "
        <span class='label label-primary'>VIP</span>
        <span class='label label-success' style='margin-left: -6px;'>{$liko_laiko}d.</span></br/><br/>
    ";
}
if($user->admin1 > time()) {  
    $liko_laiko = round(($user->admin1 - time() ) / 24 / 60 /60);
    echo "
        <span class='label label-primary'>Admin 1</span>
        <span class='label label-success' style='margin-left: -6px;'>{$liko_laiko}d.</span></br/><br/>
    ";
}
if($user->admin2 > time()) {  
    $liko_laiko = round(($user->admin2 - time() ) / 24 / 60 /60);
    echo "
        <span class='label label-primary'>Admin 2</span>
        <span class='label label-success' style='margin-left: -6px;'>{$liko_laiko}d.</span></br/><br/>
    ";
}
if($user->banned > time()) {  
    $liko_laiko = round(($user->banned - time()) / 60);
    echo "
        <span class='label label-primary'>Užblokuotas</span>
        <span class='label label-success' style='margin-left: -6px;'>{$liko_laiko}min.</span></br/><br/>
    ";
}
echo "
    </div>
";  
?>