<?php
    switch($item){
        case "vip":
            $user->buy("vip");
            break;
        case "admin1":
            $user->buy("admin1");
            break;
        case "admin2":
            $user->buy("admin2");
            break;
    }
?>
<div class='alert alert-info'>
    Jūs turite <b><?php $user->getUserInfo(); echo $user->credits; ?></b> kreditų, norint nusipirkti kreditų, spauskite <b><a href='index.php?id=buy_credits'>čia</a></b>
</div>
<div class="row">
    <div class="col-sm-8 col-md-6">
        <div class="thumbnail">
            <img src='images/vip.jpg'/>
            <div class="caption">
                <h3>VIP (30d.)</h3>
                <p>
                    <li>Gauna /vheal</li>
                    <li>Gauna 2x XP/min.</li>
                    <li>Gauna /vchat</li>
                    Kaina <b>10</b> kreditų
                </p>
                <p> 
                    <center><a href="index.php?id=shop&item=vip" class="btn btn-success" role="button"><span class='glyphicon glyphicon-ok'></span> Pirkti</a></center> 
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-md-6">
        <div class="thumbnail">
            <img src='images/admin.jpg'/>
            <div class="caption">
                <h3>ADMIN 1 (30d.)</h3>
                <p>
                    <li>Gauna /aheal</li>
                    <li>Gauna 3x XP/min.</li>
                    <li>Gauna /agun</li>
                    <li>Gauna /skin</li>
                    <li>Gauna /skelbti</li>
                    Kaina <b>30</b> kreditų
                </p>
                <p> 
                    <center><a href="index.php?id=shop&item=admin1" class="btn btn-success" role="button"><span class='glyphicon glyphicon-ok'></span> Pirkti</a></center>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-md-6">
        <div class="thumbnail">
            <img src='images/admin2.jpg'/>
            <div class="caption">
                <h3>ADMIN 2 (30d.)</h3>
                <p>
                    <li>Gauna /ahp</li>
                    <li>Gauna 5x money/min.</li>
                    <li>Gauna /sskelbti</li>
                    Kaina <b>70</b> kreditų
                </p>
                <p> 
                    <center><a href="index.php?id=shop&item=admin2" class="btn btn-success" role="button"><span class='glyphicon glyphicon-ok'></span> Pirkti</a></center> 
                </p>
            </div>
        </div>
    </div>
</div>