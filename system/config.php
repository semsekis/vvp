<?php

require('includes/class/system.class.php');
require('includes/class/auth.class.php');
require('includes/class/user.class.php');
require('includes/class/news.class.php');
require('includes/class/admin.class.php');
    
require('includes/header.php');
require('includes/navigation.php');

if(isset($_SESSION['uid'])) {     
    switch($id){
        case "news":
            require('pages/news.php');
            break;
        case "main":
            require('pages/main.php');
            break;
        case "logout":
            require('pages/logout.php');
            break;
        case "settings":
            require('pages/settings.php');
            break;
        case "shop":
            require('pages/shop.php');
            break;
        case "settings":
            require('pages/settings.php');
            break;
        case "buy_credits":
            require('pages/buy_credits.php');
            break;
        case "admin_login":
            require('pages/admin/admin_login.php');
            break;
        default:
            require('pages/news.php');
    }   
}elseif(isset($_SESSION['aid'])){
    switch($id){
        case "admin_new_settings":
            require('pages/admin/admin_new_settings.php');
            break;
        case "admin_web_settings":
            require('pages/admin/admin_web_settings.php');
            break;
        case "admin_user_settings":
            require('pages/admin/admin_user_settings.php');
            break;
        case "admin_admin_settings":
            require('pages/admin/admin_admin_settings.php');
            break;
        case "logout":
            require('pages/logout.php');
            break;
        case "admin_unban":
            $admin->unbanUser($item);
            break;
        case "admin_delete":
            $admin->deleteUser($item);
            break;
        case "admin_find_user":
            require('pages/admin/admin_find_user.php');
            break;
        case "admin_delete_new":
            $admin->deleteNew($item);
            break;
        case "admin_change_theme":
            $admin->changeTheme($item);
            break;
        default:
            require('pages/admin/admin_web_settings.php');
    }
}else{
    if($id == "admin_login") { 
        require('pages/admin/admin_login.php'); 
    }else{
        require('pages/login.php');    
    }
}




require('includes/footer.php');

?>