<?php

class admin extends system {
 
    public function __construct() {
        parent::__construct();   
    }
    
    public function login($username,$password) {
        $username = htmlspecialchars($username);
        $password = htmlspecialchars($password);
        
        var_dump($this->ConnectionError);
        
        $query = $this->PDO->prepare("SELECT `id` FROM `admin` WHERE `username`=? AND `password`=?");
        $query->execute(array($username,$password));
        
        $count = $query->rowCount();
        
        if($count == 1) {
            $info = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['aid'] = $info['id'];
            header("Location:index.php?id=admin_web_settings");
        }else{
            echo "<div class='alert alert-danger'>Neteisingas slapyvardis / slaptažodis!</div>";
        }
    }
    
    public function updateWebSettings($title,$footer) {
        $title = htmlspecialchars($title);
        $footer = htmlspecialchars($footer);
        
        if($title == "" OR $footer == "") {
            echo "<div class='alert alert-danger'>Paliktas tuščias laukelis!</div>";   
        }else{
            $query = $this->PDO->prepare("UPDATE `info` SET `title`=? , `footer`=? WHERE `id`=?");
            $query->execute(array($title,$footer,1));
            header("Location: index.php?id=admin_web_settings");
        }
    }
    public function updateAdminSettings($username,$password) {
        $username = htmlspecialchars($username);
        $password = htmlspecialchars($password);
        
        if($username == "" OR $password == "") {
            echo "<div class='alert alert-danger'>Paliktas tuščias laukelis!</div>";   
        }else{
            $query = $this->PDO->prepare("UPDATE `admin` SET `username`=? , `password`=? WHERE `id`=?");
            $query->execute(array($username,$password,1));
            header("Location: index.php?id=admin_admin_settings");
        }
    }
    public function showUsers(){
   
        $total = $this->PDO->query("SELECT COUNT(*) FROM `user` ")->fetchColumn();
        $limit = 20;
        $pages = ceil($total / $limit);
        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),
        )));
        $offset = ($page - 1)  * $limit;
        $start = $offset + 1;
        $end = min(($offset + $limit), $total);

        $prevlink = '<a href="?id=admin_user_settings&page=' . ($page - 1) . '" aria-label="Praeitas"><span aria-hidden="true">&laquo;</span></a>';
        $nextlink = '<a href="?id=admin_user_settings&page=' . ($page + 1) . '" aria-label="Sekantis"><span aria-hidden="true">&raquo;</span></a>';
        
        $stmt = $this->PDO->prepare('SELECT * FROM `user`  ORDER BY `id` DESC LIMIT :limit OFFSET :offset');
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();     
        
        if ($stmt->rowCount() > 0) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            $iterator = new IteratorIterator($stmt);       

            echo "
                <table class='table'>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Vardas_Pavardė</th>
                            <th>Užblokuotas</th>
                            <th>Atblokuoti</th>
                            <th>Trinti</th>
                        </tr>
                    </thead>
                    <tbody>
            ";
            
            foreach($iterator as $row) {
                if($row['banned'] > time()){
                    $time = round(($row['banned'] - time()) / 60);
                    $banned = "Taip, {$time}min.";   
                }else{
                    $banned = "Ne.";   
                }
                echo "
                        <tr>
                            <th>{$row['id']}</th>
                            <td>{$row['username']}</td>
                            <td>{$banned}</td>
                            <td><a href='index.php?id=admin_unban&item={$row['id']}'><span class='glyphicon glyphicon-ok'></span></a></td>
                            <td><a href='index.php?id=admin_delete&item={$row['id']}'><span class='glyphicon glyphicon-remove'></span></a></td>
                        </tr>
                ";
            }
            echo "
                    </tbody>
                </table>         
            <center>
                <nav>
                    <ul class='pagination'>
                        <li>
                            $prevlink
                        </li>
            ";

            for($i = 1; $i <= $pages; $i++ ) {
              echo ($i == $page) ? "<li class='active'><a href='index.php?id=admin_user_settings&page=$i'> $i </a></li>" : "<li><a href='index.php?id=admin_user_settings&page=$i'> $i </a></li>"; 
                
            }

            echo "
                        <li>
                            $nextlink
                        </li>
                    </ul>
                </nav>
            </center>
            ";
        } else {
            echo "<div class='alert alert-danger'>Žaidėjų nėra!</div>";
        }

        
    }
    public function showNews(){
   
        $total = $this->PDO->query("SELECT COUNT(*) FROM `news` ")->fetchColumn();
        $limit = 20;

        $pages = ceil($total / $limit);
        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),
        )));

        $offset = ($page - 1)  * $limit;
        $start = $offset + 1;
        $end = min(($offset + $limit), $total);

        $prevlink = '<a href="?id=admin_user_settings&page=' . ($page - 1) . '" aria-label="Praeitas"><span aria-hidden="true">&laquo;</span></a>';
        $nextlink = '<a href="?id=admin_user_settings&page=' . ($page + 1) . '" aria-label="Sekantis"><span aria-hidden="true">&raquo;</span></a>';
        
        $stmt = $this->PDO->prepare('SELECT * FROM `news`  ORDER BY `id` DESC LIMIT :limit OFFSET :offset');
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();
         
        if ($stmt->rowCount() > 0) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            $iterator = new IteratorIterator($stmt);       

            echo "
                <table class='table'>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pavadinimas</th>
                            <th>Autorius</th>
                            <th>Data</th>
                            <th>Trinti</th>
                        </tr>
                    </thead>
                    <tbody>
            ";
            
            foreach($iterator as $row) {
                echo "
                        <tr>
                            <th>{$row['id']}</th>
                            <td>{$row['name']}</td>
                            <td>{$row['author']}</td>
                            <td>{$row['date']}</td>
                            <td><a href='index.php?id=admin_delete_new&item={$row['id']}'><span class='glyphicon glyphicon-remove'></span></a></td>
                        </tr>
                ";
            }
            echo "
                    </tbody>
                </table>         
            <center>
                <nav>
                    <ul class='pagination'>
                        <li>
                            $prevlink
                        </li>
            ";

            for($i = 1; $i <= $pages; $i++ ) {
              echo ($i == $page) ? "<li class='active'><a href='index.php?id=admin_user_settings&page=$i'> $i </a></li>" : "<li><a href='index.php?id=admin_user_settings&page=$i'> $i </a></li>"; 
                
            }

            echo "
                        <li>
                            $nextlink
                        </li>
                    </ul>
                </nav>
            </center>
            ";
        } else {
            echo "<div class='alert alert-danger'>Naujienų nėra!</div>";
        }

        
    }
    public function deleteUser($id){
        if(ctype_digit((string) $id)) {
            $query = $this->PDO->prepare("DELETE FROM `user` WHERE `id`=?");
            $query->execute(array($id));
            header("Location: index.php?id=admin_user_settings");
        }
    }
    public function deleteNew($id){
        if(ctype_digit((string) $id)) {
            $query = $this->PDO->prepare("DELETE FROM `news` WHERE `id`=?");
            $query->execute(array($id));
            header("Location: index.php?id=admin_new_settings");
        }
    }
    public function unbanUser($id){
        if(ctype_digit((string) $id)) {
            $query = $this->PDO->prepare("UPDATE `user` SET `banned`=? WHERE `id`=?");
            $query->execute(array(0,$id));
            header("Location: index.php?id=admin_user_settings");
        }
    }
    public function findUser($username){
        $username = htmlspecialchars($username);
        $query = $this->PDO->prepare("SELECT `id` FROM `user` WHERE `username`=?");
        $query->execute(array($username));
        $c = $query->rowCount();
        if($c > 0) {
            header("Location: index.php?id=admin_find_user&item=$username");
        }else{
            echo "<div class='alert alert-danger'>Tokio žaidėjo nėra!</div>";
        }   
    }
    public function getAdminInfo(){
        $query = $this->PDO->prepare("SELECT * FROM `admin` WHERE `id`=?");
        $query->execute(array(1));
        
        $info = $query->fetch(PDO::FETCH_ASSOC);
        $this->username = $info['username'];
        $this->password = $info['password'];
    }
    public function changeTheme($item){
        if($item == "default" || $item == "primary" || $item == "danger" || $item == "warning" || $item = "success") {
            $query= $this->PDO->prepare("UPDATE `info` SET `theme`=? WHERE `id`=?");
            $query->execute(array($item,1));
            header("Location: index.php?id=admin_web_settings");
        }
    }
}
$admin = new admin;
?>