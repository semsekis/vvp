<?php

class news extends system{
    
    public function __construct() {
        parent::__construct();   
    }
    
    public function showNews(){
        
        $total = $this->PDO->query("SELECT COUNT(*) FROM `news` ")->fetchColumn();
        $limit = 3;

        $pages = ceil($total / $limit);
        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),
        )));

        $offset = ($page - 1)  * $limit;


        $start = $offset + 1;
        $end = min(($offset + $limit), $total);

        $prevlink = '<a href="?page=' . ($page - 1) . '" aria-label="Praeitas"><span aria-hidden="true">&laquo;</span></a>';
        $nextlink = '<a href="?page=' . ($page + 1) . '" aria-label="Sekantis"><span aria-hidden="true">&raquo;</span></a>';
        

        $stmt = $this->PDO->prepare('SELECT * FROM `news`  ORDER BY `id` DESC LIMIT :limit OFFSET :offset');
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();
        
        
        
        if ($stmt->rowCount() > 0) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            $iterator = new IteratorIterator($stmt);       

            foreach($iterator as $row) {
                echo "<h3> #{$row['id']} | {$row['name']} </h3>";
                echo "<p>{$row['content']}</p>";
                echo "<p align='right'>parašė <b>{$row['author']}</b>, <b>{$row['date']}</b></p><hr>";
                
            }
            echo "
            <center>
                <nav>
                    <ul class='pagination'>
                        <li>
                            $prevlink
                        </li>
            ";

            for($i = 1; $i <= $pages; $i++ ) {
              echo ($i == $page) ? "<li class='active'><a href='index.php?page=$i'> $i </a></li>" : "<li><a href='index.php?page=$i'> $i </a></li>"; 
                
            }

            echo "
                        <li>
                            $nextlink
                        </li>
                    </ul>
                </nav>
            </center>
            ";
        } else {
            echo "<div class='alert alert-danger'>Naujienų nėra!</div>";
        }

        
    }
    public function createNew($author,$name,$content) {
        if($author == "" OR $name == "" OR $content == "") {
            echo "<div class='alert alert-danger'>Paliktas tuščias laukelis!</div>";   
        }else{
            $date = date("Y-m-d | H:i:s");
            $query = $this->PDO->prepare("INSERT INTO `news` (`name`,`content`,`date`,`author`) VALUES (?,?,?,?)");
            $query->execute(array($name,$content,$date,$author));
            echo "<div class='alert alert-success'>Sekmingai!</div>";
        }
    }
}
$news = new news;

?>