<?php

class auth extends system {
    
    public function login($username,$password) {
        $username = htmlspecialchars($username);
        $password = htmlspecialchars($password);
        
        $query = $this->PDO->prepare("SELECT `id` FROM `user` WHERE `username`=? AND `password`=?");
        $query->execute(array($username,$password));
        
        $count = $query->rowCount();
        
        if($count == 1) {
            $info = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['uid'] = $info['id'];
            header("Location:index.php?id=news");
        }else{
            echo "<div class='alert alert-danger'>Neteisingas slapyvardis / slaptažodis!</div>";
        }
    }

}
$auth = new auth;