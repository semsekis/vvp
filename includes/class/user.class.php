<?php

class user extends system{
    
    public function getUserInfo(){
        $query = $this->PDO->prepare("SELECT * FROM `user` WHERE `id`=?");
        $query->execute(array($_SESSION['uid']));
        
        $info = $query->fetch(PDO::FETCH_ASSOC);
        $this->username = $info['username'];
        $this->password = $info['password'];
        $this->money = $info['money'];
        $this->job = $info['job'];
        $this->skin = "<img src='skins/{$info['skin']}.jpg'/>";
        $this->skin_id = $info['skin'];
        $this->xp = $info['xp'];
        $this->bank_money = $info['bank_money'];
        $this->credits = $info['credits'];
        $this->vip = $info['vip'];
        $this->admin1 = $info['admin1'];
        $this->admin2 = $info['admin2'];
        $this->banned = $info['banned'];
    }
    
    public function updateUserInfo($column , $value, $cost = null) {
        if($value == "") { 
            echo "<div class='alert alert-danger'>Palikote tuščia langelį! </div>";                       
        }else{
            $this->getUserInfo();
            
            if(isset($cost)) {
                if( $cost <= $this->credits ) {
                    $value = htmlentities($value);

                    $query = $this->PDO->prepare("UPDATE `user` SET $column = ? , `credits`=`credits`-? WHERE `id`=?");
                    $query->execute(array($value,$cost,$_SESSION['uid']));
                    echo "<div class='alert alert-success'>Informacija sekmingai atnaujinta! </div>";
                }else{
                    echo "<div class='alert alert-danger'>Jūms nepakanka kreditų! </div>";   
                }
            }else{
                $value = htmlentities($value);

                $query = $this->PDO->prepare("UPDATE `user` SET $column = ? WHERE `id`=?");
                $query->execute(array($value,$_SESSION['uid']));
                echo "<div class='alert alert-success'>Informacija sekmingai atnaujinta! </div>";   
            }
        }
    }
    public function buy($item){
        switch($item){
            case "vip":
                $cost = 10;
                break;
            case "admin1":
                $cost = 30;
                break;
            case "admin2":
                $cost = 70;
                break;
        }
        
        $this->getUserInfo();
        if($cost <= $this->credits){
            $query = $this->PDO->prepare("UPDATE `user` SET $item = $item + ? , `credits`=`credits`-?");
            $time = time() + 2592000;
            $query->execute(array($time,$cost));
            echo "<div class='alert alert-success'>Sekmingai nusipirkai privilegiją! </div>";
        }else{
            echo "<div class='alert alert-danger'>Jūms nepakanka kreditų! </div>";
        }
    }
}
$user = new user;

?>