<div class="panel-heading">
    <div class="panel-title">
        <center>
            <?php
            if(isset($_SESSION['uid'])) {
                echo "
                    <a href='index.php?id=news' class='btn btn-default'>Naujienos</a>
                    <a href='index.php?id=main' class='btn btn-info'>Žaidėjo informacija</a>
                    <a href='index.php?id=shop' class='btn btn-success'>Paslaugų pirkimas</a>
                    <a href='index.php?id=settings' class='btn btn-warning'>Nustatymai</a>
                    <a href='index.php?id=logout' class='btn btn-danger'><span class='glyphicon glyphicon-remove'></span></a>
                ";
            }elseif(isset($_SESSION['aid'])){
                echo "
                    <a href='index.php?id=admin_web_settings' class='btn btn-default'>Sistemos nustatymai</a>
                    <a href='index.php?id=admin_new_settings' class='btn btn-info'>Naujienų tvarkymas</a>
                    <a href='index.php?id=admin_user_settings' class='btn btn-success'>Žaidėjo tvarkymas</a>
                    <a href='index.php?id=admin_admin_settings' class='btn btn-warning'>Admin. nustatymai</a>
                    <a href='index.php?id=logout' class='btn btn-danger'><span class='glyphicon glyphicon-remove'></span></a>
                ";
            }else{
                echo "
                    Prisijungimas
                ";
            }
            ?>
        </center>
    </div>
</div>
<div class="panel-body">